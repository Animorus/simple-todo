<?php

namespace app\models;

use yii\behaviors\OptimisticLockBehavior;

/**
 * This is the model class for table "note".
 *
 * @property int $id
 * @property string|null $title
 * @property int $priority
 * @property int $done
 * @property int $version
 */
class Note extends \yii\db\ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'note';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'priority', 'done'], 'required'],
            [['title'], 'string'],
            [['done'], 'boolean'],
            [['priority'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function formName()
    {
        return '';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            OptimisticLockBehavior::class
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function optimisticLock()
    {
        return 'version';
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'priority' => 'Priority',
            'done' => 'Done',
            'version' => 'Version',
        ];
    }
}
