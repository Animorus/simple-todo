<?php

namespace app\models;

/**
 * Extends Note model for note table without OptimisticLock
 *
 * @property int $id
 * @property string|null $title
 * @property int $priority
 * @property int $done
 */

class NoteUnlocked extends Note
{
    /**
     * Scenario when Done attribute change value
     */
    const SCENARIO_TOGGLE_DONE = 'toggleDone';

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function optimisticLock()
    {
        return NULL;
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();

        $scenarios[self::SCENARIO_TOGGLE_DONE] = ['done'];

        return $scenarios;
    }
}
