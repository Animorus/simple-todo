<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\bootstrap4\ButtonGroup;

/* @var $this yii\web\View */
/* @var $model app\models\Note */
/* @var $form yii\widgets\ActiveForm */
?>


<?php $form = ActiveForm::begin([
    'id' => 'note-form',
    'action' => Url::toRoute('note/create'),
    'fieldConfig' => [
        'template' => "{label}\n{input}\n{hint}\n{error}",
    ]
]); ?>
<?= $form->field($model, 'version')->hiddenInput()->label(false) ?>
<?= $form->field($model, 'title')->textarea(['id' => false, 'rows' => 2, 'placeholder' => 'Take a note...'])->label(false) ?>

<div class="row align-items-end">
    <div class="col-8 col-lg-9">
        <?= $form->field($model, 'priority')->textInput(['id' => false,  'type' => 'number', 'class' => 'form-control form-control-sm'])  ?>
    </div>
    <div class="col-2 col-lg-1">
        <?= $form->field($model, 'done')->checkbox(['id' => false, 'class' => 'form-control form-big-checkbox'], false) ?>
    </div>
    <div class="col-2">
        <div class="form-group edit">
            <?php
            echo ButtonGroup::widget([
                'buttons' => [
                    Html::submitButton('<i class="far fa-save"></i>', ['class' => 'btn btn-secondary btn-lg']),
                    Html::button(
                        '<i class="fas fa-trash-alt"></i>',
                        [
                            'class' => 'btn btn-danger btn-lg note-action',
                            'data' => [
                                'action' => 'delete'
                            ]
                        ]
                    )
                ],
                'options' => [
                    'class' => 'w-100'
                ]
            ]);
            ?>
        </div>

        <div class="form-group create">
            <?= Html::submitButton('<i class="far fa-save"></i>', ['class' => 'btn btn-secondary btn-lg w-100']) ?>
        </div>
    </div>
</div>




<?php ActiveForm::end(); ?>