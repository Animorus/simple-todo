<?php
/* @var $this yii\web\View */
/* @var $model app\models\Note */
?>

<div class="card bg-dark <?= $model->done ? 'note-done text-white-50' : 'text-white' ?>">
    <div class="card-body">
        <p class="card-title"><?= $model->title ?></p>
    </div>
    <div class="card-toolbar">
        <div class="note-action note-action" data-action="edit">
            <i class="fas fa-edit"></i> <span>Edit</span>
        </div>
        <div class="note-action note-action" data-action="mark">
            <i class="fas fa-calendar-check"></i>
            <span><?= $model->done ? 'Undone' : 'Done' ?></span>
        </div>
        <div class="note-action note-action" data-action="delete">
            <i class="fas fa-trash-alt"></i>
            <span>Delete</span>
        </div>
    </div>
</div>