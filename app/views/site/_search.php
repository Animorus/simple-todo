<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\NoteSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="note-search mt-3">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get'
    ]); ?>

    <div class="d-flex">
        <?= $form->field($model, 'title', ['options' => ['class' => 'w-100']])
            ->textInput(['class' => 'form-control form-control-lg', 'placeholder' => 'Search...'])
            ->label(false) ?>

        <div class="form-group btn-group text-white pl-2">
            <?= Html::submitButton('<i class="fas fa-search"></i>', ['class' => 'btn btn-outline-light btn-lg']) ?>
            <?= Html::button('<i class="fas fa-cogs"></i>', [
                'data' => [
                    'toggle' => 'collapse',
                    'target' => '#search-settings'
                ],
                'class' => ['btn btn-outline-light btn-lg'],
                'aria-expanded' => 'false',
                'aria-controls' => 'search-settings'
            ]) ?>
        </div>
    </div>

    <div id="search-settings" class="collapse">
        <div class="card card-body bg-dark text-white">
            <?= $form->field($model, 'priority')->textInput(['class' => 'form-control form-control-sm', 'type' => 'number']) ?>
            <?= $form->field($model, 'done')->checkbox() ?>
        </div>
    </div>


    <?php ActiveForm::end(); ?>

</div>