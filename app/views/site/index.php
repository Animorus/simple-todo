<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\Pjax;
use yii\bootstrap4\Modal;
/* @var $this yii\web\View */
/* @var $searchModel app\models\NoteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Notes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="note-index">
    <div class="row">
        <div class="note-create card card-body bg-dark text-white col-md-8 offset-md-2 mt-2 pt-2">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>

    <?=$this->render('_search', ['model' => $searchModel]); ?>

    <?php Pjax::begin(['id' => 'note-pjax']); ?>

    <?= ListView::widget([
        'id' => 'note-list',
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'note col-sm-6 col-md-4 col-lg-3 my-2'],
        'layout' => '<div class="row">{items}</div> {pager}',
        'itemView' => 'item',
    ]) ?>

    <?php Pjax::end(); ?>

</div>

<?php
Modal::begin([
    'id' => 'note-modal',
    'title' => '',
    'footer' => '',
    'size' => 'modal-lg',
    'toggleButton' => false,
]);
?>

<?php Modal::end(); ?>