<?php

namespace app\controllers;

use Yii;
use app\models\Note;
use app\models\NoteUnlocked;
use yii\rest\ActiveController;
use yii\web\NotFoundHttpException;

class NoteController extends ActiveController
{
    /**
     * {@inheritdoc}
     */
    public $modelClass = 'app\models\Note';

    /**
     * {@inheritdoc}
     */
    public function runAction($id, $params = [])
    {
        try {
            return parent::runAction($id, $params);
        } catch (yii\db\StaleObjectException $th) {
            Yii::$app->response->setStatusCode(412);
        }
    }

    /**
     * Toggle Done value (0 or 1)
     * @return Note the loaded model
     * @throws NotFoundHttpException if model cannot be found
     */
    public function actionToggleDone($id)
    {
        $model = $this->findUnlocked($id);
        $model->scenario = NoteUnlocked::SCENARIO_TOGGLE_DONE;
        $model->done = (int)!$model->done;
        $model->save();

        return $model;
    }


    /**
     * Finds the Note model based on its primary key value without OptimisticLock.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Note the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findUnlocked($id)
    {
        if (($model = NoteUnlocked::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
