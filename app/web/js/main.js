


var Note = function (settings = null) {
    settings = $.extend({
        formContainer: '#note-form',
        pjaxContainer: '#note-pjax',
        modalContainer: '#note-modal'
    }, settings);

    var apiUrl = $(settings.formContainer).attr('action');

    this.createItem = function (item) {
        ajaxRequest({
            url: `${apiUrl}`,
            data: item,
            block: $('.note-create'),
            type: 'POST',
        });
    };

    this.editItem = function (id, item) {
        let element = $(`[data-key=${id}]`);
        let url = `${apiUrl}/${id}?suppress_response_code`;

        ajaxRequest({
            url: url,
            data: item,
            block: element,
            type: 'PATCH',
            success: function () {
                $(settings.modalContainer).modal('hide');
            },
            error: function (jqXHR, exception) {
                if (jqXHR.status == 412) {
                    let btnGroup = $('<div />').addClass('btn-group')
                        .append(
                            $('<button />').addClass('btn btn-dark note-action').attr('data-action', 'edit').text('Edit again')
                        )
                        .append(
                            $('<button />').addClass('btn btn-danger').attr('data-dismiss', 'modal').text('Cancel')
                        );

                    showModal(
                        'Oops! Something gone wrong',
                        'Conflict, item was changed by another user, your changes will be lost.',
                        btnGroup
                    );
                }
            },
        });
    };

    this.toggleDone = function (id) {
        let element = $(`.note[data-key=${id}]`);
        let url = `${apiUrl}/toggle-done`;

        ajaxRequest({
            url: url,
            type: 'GET',
            data: {
                id: id
            },
            block: element
        });
    };

    this.deleteItem = function (id) {
        let element = $(`[data-key=${id}]`);
        let url = `${apiUrl}/${id}`;

        ajaxRequest({
            url: url,
            type: 'DELETE',
            block: element,
            success: function () {
                $(settings.modalContainer).modal('hide');
            }
        });
    };

    this.showEdit = function (id) {
        let url = `${apiUrl}/${id}`;
        let element = $(`.note[data-key=${id}]`);

        ajaxRequest({
            url: url,
            type: 'GET',
            block: element,
            success: function (data) {
                let formClone = $(settings.formContainer).clone();

                $.each(data, function (key, value) {
                    let inputElement = formClone.find(`[name='${key}']`);
                    if (inputElement.is(':checkbox')) {
                        inputElement.prop('checked', value);
                    } else {
                        inputElement.val(value)
                    }
                });

                formClone.attr('id', 'note-edit');

                showModal(`Edit record #${id}`, formClone).attr('data-key', id);
            }
        });
    }

    function showModal(title, body, footer = null) {
        let showFooter = (footer != null);
        let modalElement = $(settings.modalContainer);

        modalElement.find('.modal-title').html(title);
        modalElement.find('.modal-body').html(body);

        modalElement.find('.modal-footer').toggle(showFooter);
        if (showFooter) {
            modalElement.find('.modal-footer').html(footer);
        }

        return modalElement.modal('show');
    }

    function changeLoading(element, state = true) {
        if (!element instanceof jQuery) element = $(element);

        if (state) {
            $('<div class="spinner-border" role="status"><span class="sr-only">Loading...</span></div>').appendTo(element);
            element.addClass('loading');
        }
        else {
            element.removeClass('loading');
            element.find('.spinner-border').remove();
        }
    }

    function ajaxRequest(params, callback = null) {
        if (params.block) {
            changeLoading(params.block, true);
        }

        return $.ajax(params).always(function (result) {
            if (callback == null) {
                $.pjax.reload({ container: settings.pjaxContainer, async: false });
            } else {
                callback(result)
            }

            if (params.block) {
                changeLoading(params.block, false);
            }
        });
    }
};

$(function () {
    var noteHelper = new Note();

    $('body').on('click', '.note-action', function () {
        let action = $(this).attr('data-action');
        let id = $(this).parents('[data-key]').attr('data-key');

        switch (action) {
            case 'delete':
                noteHelper.deleteItem(id);
                break;
            case 'mark':
                noteHelper.toggleDone(id);
                break;
            case 'edit':
                noteHelper.showEdit(id);
                break;
            default:
                break;
        }
    });

    $('body').on('submit', '#note-form', function (e) {
        e.preventDefault();
        let item = $(this).serializeArray();
        noteHelper.createItem(item)
    });

    $('body').on('submit', '#note-edit', function (e) {
        e.preventDefault();
        let id = $(this).parents('[data-key]').attr('data-key');
        let item = $(this).serializeArray();
        noteHelper.editItem(id, item);
    });
});