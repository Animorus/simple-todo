<?php

use yii\db\Migration;

/**
 * Handles the creation of table `note`.
 */
class m211114_115428_create_note_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('note', [
            'id' => $this->primaryKey(),
            'title' => $this->text()->notNull(),
            'priority' => $this->integer()->notNull()->defaultValue(0),
            'done' => $this->boolean()->notNull()->defaultValue(false),
            'version' => $this->integer()->notNull()->defaultValue(0),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('note');
    }
}
