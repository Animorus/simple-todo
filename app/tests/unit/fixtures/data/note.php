<?php

return [
    'note0' => [
        'title' => 'Aut voluptas iusto nam incidunt dolores voluptate.',
        'done' => false,
    ],
    'note1' => [
        'title' => 'Delectus in voluptatum necessitatibus impedit illo velit corporis.',
        'done' => false,
    ],
    'note2' => [
        'title' => 'Harum assumenda ut est neque quas.',
        'done' => false,
    ],
    'note3' => [
        'title' => 'Minima assumenda id dolor et nemo.',
        'done' => false,
    ],
    'note4' => [
        'title' => 'Sequi pariatur non debitis sit.',
        'done' => true,
    ],
    'note5' => [
        'title' => 'Accusantium culpa esse cupiditate est aut est.',
        'done' => false,
    ],
    'note6' => [
        'title' => 'Dicta voluptas voluptate ut excepturi veniam consequatur.',
        'done' => false,
    ],
    'note7' => [
        'title' => 'Temporibus omnis ut est molestiae vel harum non aut.',
        'done' => true,
    ],
    'note8' => [
        'title' => 'Dicta vitae et quam corporis.',
        'done' => false,
    ],
    'note9' => [
        'title' => 'Minus magni et rerum eum voluptatem.',
        'done' => true,
    ],
    'note10' => [
        'title' => 'Iusto ipsum ducimus voluptatem veniam dolorem aut odit est.',
        'done' => false,
    ],
    'note11' => [
        'title' => 'Doloremque ea facilis expedita veniam.',
        'done' => true,
    ],
    'note12' => [
        'title' => 'Quia necessitatibus nisi enim fugit harum incidunt laudantium.',
        'done' => true,
    ],
    'note13' => [
        'title' => 'Et sed molestiae nemo qui dolores animi.',
        'done' => true,
    ],
    'note14' => [
        'title' => 'Commodi enim tenetur officiis qui consectetur ut voluptatem.',
        'done' => true,
    ],
    'note15' => [
        'title' => 'Maxime consequuntur saepe omnis aliquam aut.',
        'done' => true,
    ],
    'note16' => [
        'title' => 'Et voluptates repudiandae rerum minima.',
        'done' => true,
    ],
    'note17' => [
        'title' => 'Corporis iusto dolorem dolorem consequuntur eum aut soluta.',
        'done' => true,
    ],
    'note18' => [
        'title' => 'Qui doloremque molestias aut itaque sit.',
        'done' => true,
    ],
    'note19' => [
        'title' => 'Neque quibusdam officiis quaerat dicta.',
        'done' => true,
    ],
    'note20' => [
        'title' => 'Fuga harum accusamus ad quas nemo qui distinctio.',
        'done' => true,
    ],
    'note21' => [
        'title' => 'Ea ea cumque nemo qui iste.',
        'done' => false,
    ],
    'note22' => [
        'title' => 'Debitis illum quaerat quidem aut.',
        'done' => false,
    ],
    'note23' => [
        'title' => 'Libero accusantium aperiam sapiente dolores.',
        'done' => false,
    ],
    'note24' => [
        'title' => 'Hic ut ipsum alias.',
        'done' => true,
    ],
    'note25' => [
        'title' => 'Omnis omnis sed nobis magni dolor mollitia voluptas.',
        'done' => false,
    ],
    'note26' => [
        'title' => 'Recusandae fugiat minus omnis tempore.',
        'done' => true,
    ],
    'note27' => [
        'title' => 'Voluptatem aut est similique mollitia rerum quasi temporibus.',
        'done' => true,
    ],
    'note28' => [
        'title' => 'Dicta impedit quidem voluptas et.',
        'done' => false,
    ],
    'note29' => [
        'title' => 'Odio qui aut aut rerum.',
        'done' => true,
    ],
    'note30' => [
        'title' => 'Quia laboriosam consequatur aut aperiam quasi doloremque.',
        'done' => true,
    ],
    'note31' => [
        'title' => 'Qui esse quas consequatur adipisci aut non.',
        'done' => true,
    ],
    'note32' => [
        'title' => 'Laborum animi voluptas nobis quo illo.',
        'done' => true,
    ],
    'note33' => [
        'title' => 'Quae et culpa deserunt atque illo nam.',
        'done' => false,
    ],
    'note34' => [
        'title' => 'Nostrum sed sint et impedit libero.',
        'done' => true,
    ],
    'note35' => [
        'title' => 'Nobis sit dolorem deleniti est.',
        'done' => true,
    ],
    'note36' => [
        'title' => 'Velit doloremque quia rerum in.',
        'done' => true,
    ],
    'note37' => [
        'title' => 'Id quidem at sit adipisci et.',
        'done' => false,
    ],
    'note38' => [
        'title' => 'Necessitatibus tempore occaecati voluptatibus.',
        'done' => true,
    ],
    'note39' => [
        'title' => 'Quis dolor itaque voluptas incidunt eos.',
        'done' => false,
    ],
];
