# How to start?

Build docker image

    docker-compose build

Start the container

    docker-compose up -d

Update your vendor packages

    docker-compose exec php composer update --prefer-dist
    
Run the installation triggers (creating cookie validation code)

    docker-compose exec php composer install

Apply migrations

    docker-compose exec php php /app/yii migrate --interactive=0

Generate fixtures

    docker-compose exec php php yii fixture/generate note --count=40

Load records

    docker-compose exec php php yii fixture/load Note